import { Component, OnInit } from '@angular/core';
import { ApiPersonasService } from 'src/app/servicios/api-personas.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  nombre: string = "";

  constructor(private apiPersonas: ApiPersonasService) { }

  ngOnInit() {
  }

  guardar() {
    this.apiPersonas.guardar(this.nombre);    
    alert("Nombre guardado");
    this.nombre = "";
  }

}
