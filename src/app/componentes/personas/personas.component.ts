import { Component, OnInit } from '@angular/core';
import { ApiPersonasService } from 'src/app/servicios/api-personas.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {

  personas: any[];  

  constructor(private apiPersonas: ApiPersonasService) { }

  ngOnInit() {
    this.apiPersonas.getPersonas().subscribe( resp => {
      console.log(resp.results);
      this.personas = resp.results;      
    });
  }

}
