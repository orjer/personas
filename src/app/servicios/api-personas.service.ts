import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiPersonasService {

  base_path = 'https://randomapi.com/api/niq1kg3o?key=CF4Y-58O0-FKEV-8MY2';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor( private http: HttpClient ) { }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('Ocurrión un error:', error.error.message);
    }else{
      console.error(
        `Código de error ${error.status}, ` +
        `Mensaje: ${error.error}`);
    }
    return throwError('Ocurrió un error; favor de intentar más tarde.');
}

getPersonas(): Observable<any> {
  return this.http
    .get(this.base_path + "&results=5" )
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

  guardar(nombre) {    
    localStorage.setItem("nombre", nombre);    
  }
}

